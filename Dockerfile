FROM golang:alpine as base
RUN apk update && apk upgrade && apk add --no-cache git ca-certificates g++ && update-ca-certificates 2>/dev/null || true
WORKDIR /repos/twitch-client
ADD go.mod go.sum ./
RUN go mod download

FROM base as builder
WORKDIR /repos/twitch-client
ADD . .
RUN CGO_ENABLED=0 GOOS=linux go build -o twitch-client

FROM scratch as release
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/ca-certificates.crt
COPY --from=builder /repos/twitch-client/twitch-client /twitch-client
EXPOSE 8080
ENTRYPOINT ["/twitch-client"]
